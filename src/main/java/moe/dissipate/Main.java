package moe.dissipate;

import com.google.gson.Gson;
import moe.dissipate.FFLogsConnect.EncounterSet;
import moe.dissipate.FFLogsConnect.FFLogsConnect;
import moe.dissipate.FFLogsConnect.HiddenException;
import moe.dissipate.ItemLevelsLibrary.ILLCharacter;
import moe.dissipate.ItemLevelsLibrary.Player;
import moe.dissipate.ItemLevelsLibrary.PlayerSearch;
import org.jsoup.HttpStatusException;
import spark.ModelAndView;
import spark.template.jade.JadeTemplateEngine;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static spark.Spark.*;
import static spark.debug.DebugScreen.enableDebugScreen;

public class Main {
    public static void main(String[] args){
        enableDebugScreen();
        port(getHerokuAssignedPort());
        staticFiles.location("/public");
        Map<String, Object> model = new HashMap<>();
        Gson gson = new Gson();

        get("/", (request, response) -> new ModelAndView(model, "index"), new JadeTemplateEngine());

        get("/search", (request, response) -> {
            HashMap<String, Object> temp = new HashMap<>();
            List<Player> players = null;
            try {
                players = PlayerSearch.Search(request.queryParams("Character"), request.queryParams("World") == null ?
                        "" : request.queryParams("World"));
            } catch (IOException ex){
                return timeoutError(ex);
            }
            if(players!= null) {
                if (players.size() == 1)
                    response.redirect(players.get(0).getURL());
                temp.put("isEmpty", players.isEmpty());
            }
            temp.put("characters", players);
            return new ModelAndView(temp, "search");
        }, new JadeTemplateEngine());

        get("/character/:slug", (request, response) -> {
            HashMap<String, Object> temp = new HashMap<>();
            try {
                ILLCharacter chara = new ILLCharacter(request.params(":slug"));
                temp.put("character", chara);
                try {
                    EncounterSet encounterSet = new EncounterSet(FFLogsConnect.getClearedInstances(chara.getcName(), chara.getServer(), gson));
                    temp.put("Cleared", encounterSet);
                }catch (HiddenException ex){
                    temp.put("hidden", true);
                }
                return new ModelAndView(temp, "character");
            }catch (HttpStatusException ex){
                temp.put("error", ex.getMessage());
                return new ModelAndView(temp, "misc-error");
            }
        }, new JadeTemplateEngine());

        get("/about", (request, response) -> new ModelAndView(model, "about"), new JadeTemplateEngine());
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

    private static ModelAndView timeoutError(IOException ex){
        HashMap<String, Object> model = new HashMap<>();
        model.put("Error", ex.getMessage());
        return new ModelAndView(model, "connectionError");
    }


}
