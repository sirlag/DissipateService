package moe.dissipate.FFLogsConnect;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Report {

    public Integer encounter;
    public Integer rank;
    public Integer outOf;
    public Double total;
    @SerializedName("class")
    public Integer _class;
    public Integer spec;
    public String guild;
    public Integer duration;
    public Double startTime;
    public Double itemLevel;
    public Integer patch;
    public String reportID;
    public Integer fightID;
    public Integer difficulty;
    public Integer size;
    public Boolean estimated;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Integer getEncounter() {
        return encounter;
    }

    @Override
    public String toString() {
        return "Report{" +
                "encounter=" + encounter +
                ", rank=" + rank +
                ", outOf=" + outOf +
                ", total=" + total +
                ", _class=" + _class +
                ", spec=" + spec +
                ", guild='" + guild + '\'' +
                ", duration=" + duration +
                ", startTime=" + startTime +
                ", itemLevel=" + itemLevel +
                ", patch=" + patch +
                ", reportID='" + reportID + '\'' +
                ", fightID=" + fightID +
                ", difficulty=" + difficulty +
                ", size=" + size +
                ", estimated=" + estimated +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

}

