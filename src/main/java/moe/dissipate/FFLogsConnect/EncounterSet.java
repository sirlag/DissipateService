package moe.dissipate.FFLogsConnect;

import java.util.List;

public class EncounterSet {
    private boolean hummelfaust, ratfinx, cuffOfTheSon, quickthinx, bruteJustice;

    public EncounterSet(List<Integer> clears) {
        hummelfaust = clears.contains(5006);
        ratfinx = clears.contains(26);
        cuffOfTheSon = clears.contains(27);
        quickthinx = clears.contains(28);
        bruteJustice = clears.contains(29);
    }

    public static boolean containsClear(List<Integer> clears, Integer encounterID){
        return clears.contains(encounterID);
    }

    public boolean isHummelfaust() {
        return hummelfaust;
    }

    public boolean isRatfinx() {
        return ratfinx;
    }

    public boolean isCuffOfTheSon() {
        return cuffOfTheSon;
    }

    public boolean isQuickthinx() {
        return quickthinx;
    }

    public boolean isBruteJustice() {
        return bruteJustice;
    }

    @Override
    public String toString() {
        return "EncounterSet{" +
                "hummelfaust=" + hummelfaust +
                ", ratfinx=" + ratfinx +
                ", cuffOfTheSon=" + cuffOfTheSon +
                ", quickthinx=" + quickthinx +
                ", bruteJustice=" + bruteJustice +
                '}';
    }
}
