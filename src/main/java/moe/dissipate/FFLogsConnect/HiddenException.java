package moe.dissipate.FFLogsConnect;

public class HiddenException extends Exception{
    public HiddenException() {
    }

    public HiddenException(String message) {
        super(message);
    }

    public HiddenException(String message, Throwable cause) {
        super(message, cause);
    }

    public HiddenException(Throwable cause) {
        super(cause);
    }

    public HiddenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
