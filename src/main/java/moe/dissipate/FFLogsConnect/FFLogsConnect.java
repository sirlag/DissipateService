package moe.dissipate.FFLogsConnect;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class FFLogsConnect {

    public static List<Integer> getClearedInstances(String character, String server, Gson gson) throws HiddenException{
        StringBuilder sb = new StringBuilder();
        sb.append("https://www.fflogs.com:443/v1/rankings/character/");
        sb.append(character.replaceAll(" ", "%20"));
        sb.append("/");
        sb.append(server);
        sb.append("/");
        sb.append(findRegion(server));
        sb.append("?api_key="+System.getenv("FFLOGSAPIKEY"));

        InputStream input = null;
        Reader reader = null;
        try {
            input = new URL(sb.toString()).openStream();
            reader = new InputStreamReader(input, "UTF-8");
            PushbackReader pr = new PushbackReader(reader);
            char start = (char)pr.read();
            if (start == '{')
                throw new HiddenException("This player is Hidden");
            else
                pr.unread(start);
            List<Integer> clearedInstances = new ArrayList<>();
            Type collectionType = new TypeToken<Collection<Report>>(){}.getType();
            Collection<Report> reports = gson.fromJson(pr, collectionType);
            reports.forEach(report -> clearedInstances.add(report.getEncounter()));
            return clearedInstances;
        }catch (IOException ignored) {
        }
        return new ArrayList<>();
    }

    private static String findRegion(String server){
        List<String> jp = Arrays.asList("Aegis","Alexander","Anima","Asura","Atomos","Bahamut","Belias","Carbuncle",
                "Chocobo","Durandal","Fenrir","Garuda","Gungnir","Hades","Ifrit","Ixion","Kujata","Mandragora",
                "Masamune","Pandaemonium","Ramuh","Ridill","Shinryu","Tiamat","Titan","Tonberry","Typhon","Ultima",
                "Unicorn","Valefor","Yojimbo","Zeromus");
        List<String> na = Arrays.asList("Adamantoise","Balmung","Behemoth","Brynhildr","Cactuar","Coeurl","Diabolos",
                "Excalibur","Exodus","Faerie","Famfrit","Gilgamesh","Goblin","Hyperion","Jenova","Lamia","Leviathan",
                "Malboro","Mateus","Midgardsormr","Sargatanas","Siren","Ultros","Zalera");
        List<String> eu = Arrays.asList("Cerberus","Lich","Moogle","Odin","Phoenix","Ragnarok","Shiva","Zodiark");
        if (jp.contains(server))
                return "JP";
        if (na.contains(server))
            return "NA";
        if (eu.contains(server))
            return "EU";
        return null;
    }
}

